package ru.tsc.tambovtsev.tm.listener.domain;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.api.endpoint.IDomainEndpoint;
import ru.tsc.tambovtsev.tm.listener.data.AbstractDataListener;

@Getter
@Setter
@Component
public abstract class AbstractDomainListener extends AbstractDataListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @NotNull
    protected static final String FILE_FASTERXML_JSON = "./data-fasterxml.json";

    @NotNull
    protected static final String FILE_FASTERXML_XML = "./data-fasterxml.xml";

    @NotNull
    protected static final String FILE_FASTERXML_YAML = "./data-fasterxml.yaml";

    @NotNull
    protected static final String FILE_JAXB_XML = "./data-jaxb.xml";

    @NotNull
    protected static final String FILE_JAXB_JSON = "./data-jaxb.json";

    @NotNull
    protected static final String JAVAX_XML_BIND_CONTEXT_FACTORY = "javax.xml.bind.context.factory";

    @NotNull
    protected static final String ORG_ECLIPSE_PERSISTENCE_JAXB_JAXBCONTEXT_FACTORY = "org.eclipse.persistence.jaxb.JAXBContextFactory";

    @NotNull
    protected static final String APPLICATION_JSON = "application/json";

    @Nullable
    public String getArgument() {
        return null;
    }

}
