package ru.tsc.tambovtsev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.tsc.tambovtsev.tm.dto.request.UserLogoutRequest;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.event.ConsoleEvent;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-logout";

    @NotNull
    public static final String DESCRIPTION = "Logout user profile.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @EventListener(condition = "@userLogoutListener.getName() == #event.name")
    public void handlerConsole(@NotNull final ConsoleEvent event) {
        System.out.println("[USER LOGOUT]");
        getAuthEndpoint().logout(new UserLogoutRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
