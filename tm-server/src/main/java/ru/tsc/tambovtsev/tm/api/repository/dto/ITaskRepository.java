package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

public interface ITaskRepository extends IOwnerRepository<TaskDTO> {

    void deleteByUserIdAndProjectId(@Nullable String userId, @Nullable String projectId);

}
