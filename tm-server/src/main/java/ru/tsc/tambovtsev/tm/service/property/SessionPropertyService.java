package ru.tsc.tambovtsev.tm.service.property;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.property.ISessionPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class SessionPropertyService implements ISessionPropertyService {

    @Value("${environment['session.key']:6363453453}")
    private String sessionKey;

    @Value("${environment['session.timeout']:86400}")
    private Integer sessionTimeout;

    @Value("${environment['password.iteration']:12345}")
    private Integer passwordIteration;

    @Value("${environment['password.secret']:356585985}")
    private String passwordSecret;

}
