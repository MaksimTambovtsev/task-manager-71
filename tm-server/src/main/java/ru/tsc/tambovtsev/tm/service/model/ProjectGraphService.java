package ru.tsc.tambovtsev.tm.service.model;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectGraphRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.*;

@Service
public class ProjectGraphService extends AbstractUserOwnedService<Project, IProjectGraphRepository> implements IProjectService {

    @Nullable
    @Autowired
    private IProjectGraphRepository repository;

    @NotNull
    @Override
    public IProjectGraphRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Project project = repository.findByUserIdAndId(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        repository.save(project);
        return project;
    }

}
