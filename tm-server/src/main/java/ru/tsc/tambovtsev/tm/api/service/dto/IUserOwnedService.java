package ru.tsc.tambovtsev.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.tambovtsev.tm.enumerated.SortTable;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IService<M> {

    void clear();

    void clear(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable SortTable sortTable);

    long getSize(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    @Nullable
    M findById(@Nullable String userId, @Nullable String id);

}
