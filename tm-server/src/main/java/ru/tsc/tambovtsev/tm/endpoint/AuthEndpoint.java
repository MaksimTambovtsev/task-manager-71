package ru.tsc.tambovtsev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint;
import ru.tsc.tambovtsev.tm.dto.request.UserLoginRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserLogoutRequest;
import ru.tsc.tambovtsev.tm.dto.request.UserProfileRequest;
import ru.tsc.tambovtsev.tm.dto.response.UserLoginResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserLogoutResponse;
import ru.tsc.tambovtsev.tm.dto.response.UserProfileResponse;
import ru.tsc.tambovtsev.tm.dto.model.SessionDTO;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@NoArgsConstructor
@WebService(endpointInterface = "ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpoint")
public class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLoginRequest request
    ) {
        @NotNull final String token = getAuthService().login(request.getLogin(), request.getPassword());
        return new UserLoginResponse(token);
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserLogoutRequest request
    ) {
        check(request);
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse profile(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull UserProfileRequest request
    ) {
        @Nullable final SessionDTO session = check(request);
        @Nullable final UserDTO user = getServiceLocator().getUserServiceDTO().findById(session.getUserId());
        return new UserProfileResponse(user);
    }
}
