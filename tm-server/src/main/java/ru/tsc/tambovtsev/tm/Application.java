package ru.tsc.tambovtsev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.tambovtsev.tm.component.Bootstrap;
import ru.tsc.tambovtsev.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.run();
    }

}