package ru.tsc.tambovtsev.tm.unit.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import ru.tsc.tambovtsev.tm.api.service.IProjectService;
import ru.tsc.tambovtsev.tm.config.DatabaseConfiguration;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.Project;
import ru.tsc.tambovtsev.tm.util.UserUtil;

import java.util.Collection;

@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class ProjectServiceTest {

    @NotNull
    @Autowired
    private IProjectService service;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final Project project = new Project("Project1", "Description");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        project.setUserId(UserUtil.getUserId());
        service.save(project);
    }

    @After
    public void clean() {
        service.clear(UserUtil.getUserId());
    }

    @Test
    public void createTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.create(userId);
        Assert.assertEquals(2, service.count(userId));
    }

    @Test
    public void saveTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @NotNull final Project project2 = new Project("Project2", "Description2");
        project2.setUserId(userId);
        service.save(project2);
        Assert.assertEquals(2, service.count(userId));
        @NotNull final Project projectFind = service.findById(userId, project2.getId());
        Assert.assertEquals(project2.getId(), projectFind.getId());
    }

    @Test
    public void findByIdTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final Project project2 = service.findById(userId, project.getId());
        Assert.assertNotNull(project2);
        Assert.assertEquals(project2.getId(), project.getId());
    }

    @Test
    public void findAllTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @Nullable final Collection<Project> projectList = service.findAll(userId);
        Assert.assertNotNull(projectList);
        Assert.assertEquals(1, projectList.size());
    }

    @Test
    public void clearTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.clear(userId);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeByIdTest() {
        @NotNull final String userId = UserUtil.getUserId();
        service.removeById(userId, project.getId());
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void removeTest() {
        @NotNull final String userId = UserUtil.getUserId();
        @NotNull final Project project2 = service.findById(userId, project.getId());
        service.remove(project2);
        Assert.assertEquals(0, service.count(userId));
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, service.count(UserUtil.getUserId()));
    }

}
