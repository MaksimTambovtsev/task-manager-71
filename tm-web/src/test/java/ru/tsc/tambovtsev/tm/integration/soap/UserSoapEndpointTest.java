package ru.tsc.tambovtsev.tm.integration.soap;

import lombok.SneakyThrows;
import org.apache.cxf.helpers.CastUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.tsc.tambovtsev.tm.api.endpoint.IAuthEndpointImpl;
import ru.tsc.tambovtsev.tm.api.endpoint.IUserEndpointImpl;
import ru.tsc.tambovtsev.tm.client.AuthSoapEndpointClient;
import ru.tsc.tambovtsev.tm.client.UserSoapEndpointClient;
import ru.tsc.tambovtsev.tm.marker.IntegrationCategory;
import ru.tsc.tambovtsev.tm.model.User;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Category(IntegrationCategory.class)
public class UserSoapEndpointTest {

    @NotNull
    private static IAuthEndpointImpl authEndpoint;

    @NotNull
    private static IUserEndpointImpl userEndpoint;

    @Nullable
    private static String sessionId = null;

    @NotNull
    private static final String URL = "http://localhost:8080";

    @NotNull
    private final User user = new User("User1", "fre4334feevsd3", "user1@us.com");

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthSoapEndpointClient.getInstance(URL);
        Assert.assertTrue(authEndpoint.login("admin", "admin").isValue());
        userEndpoint = UserSoapEndpointClient.getInstance(URL);
        @NotNull final BindingProvider authBindingProvider = (BindingProvider) authEndpoint;
        @NotNull final BindingProvider userBindingProvider = (BindingProvider) userEndpoint;
        @NotNull Map<String, List<String>> headers =
                CastUtils.cast(
                        (Map) authBindingProvider.getResponseContext().get(MessageContext.HTTP_RESPONSE_HEADERS)
                );
        if (headers == null) headers = new HashMap<String, List<String>>();
        @NotNull final Object cookieValue = headers.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> cookies = (List<String>) cookieValue;
        headers.put("Cookie", Collections.singletonList(cookies.get(0)));
        userBindingProvider.getRequestContext().put(MessageContext.HTTP_REQUEST_HEADERS, headers);
    }

    @AfterClass
    public static void logout() {
        authEndpoint.logout();
    }

    @Before
    public void initTest() {
        userEndpoint.save(user);
    }

    @After
    @SneakyThrows
    public void clean() {
        userEndpoint.delete(user);
    }

    @Test
    public void findAllTest() {
        @Nullable final List<User> users = userEndpoint.findAll();
        Assert.assertNotNull(users);
        Assert.assertEquals(1, users.size());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final User user2 = userEndpoint.findByLogin(user.getLogin());
        Assert.assertNotNull(user2);
        Assert.assertEquals(user2.getId(), user.getId());
    }

    @Test
    public void deleteTest() {
        clean();
        Assert.assertEquals(0, userEndpoint.count());
    }

    @Test
    public void deleteByLoginTest() {
        userEndpoint.deleteByLogin(user.getLogin());
        Assert.assertEquals(0, userEndpoint.count());
    }

    @Test
    public void countTest() {
        Assert.assertEquals(1, userEndpoint.count());
    }
    
}
