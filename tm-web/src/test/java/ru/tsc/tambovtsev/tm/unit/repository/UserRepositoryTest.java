package ru.tsc.tambovtsev.tm.unit.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.config.DatabaseConfiguration;
import ru.tsc.tambovtsev.tm.marker.UnitCategory;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

@Transactional
@WebAppConfiguration
@Category(UnitCategory.class)
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatabaseConfiguration.class})
public class UserRepositoryTest {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private AuthenticationManager manager;

    @NotNull
    private final User user1 = new User("User1", "23r32r23r0", "qwerty1@qw.com");

    @NotNull
    private final User user2 = new User("User2", "vr34r34fwe23211w", "qwerty2@qw.com");

    @Before
    public void init() {
        @NotNull final UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken("test", "test");
        @NotNull final Authentication authentication = manager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        repository.save(user1);
        repository.save(user2);
    }

    @After
    public void clean() {
        repository.deleteAll();
    }

    @Test
    public void findAllTest() {
        @Nullable final List<User> userList = repository.findAll();
        Assert.assertEquals(4, userList.size());
    }

    @Test
    public void findByLoginTest() {
        @Nullable final User user = repository.findByLogin(user1.getLogin());
        Assert.assertNotNull(user);
        Assert.assertEquals(user.getId(), user1.getId());
    }

    @Test
    public void deleteByLoginTest() {
        repository.deleteByLogin(user1.getLogin());
        Assert.assertEquals(3, repository.findAll().size());
    }

}
