package ru.tsc.tambovtsev.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.IUserRepository;
import ru.tsc.tambovtsev.tm.enumerated.Role;
import ru.tsc.tambovtsev.tm.model.CustomUser;
import ru.tsc.tambovtsev.tm.model.User;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailService")
public class UserDetailServiceBean implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private IUserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final User user = findByLogin(username);
        if (user == null) throw new UsernameNotFoundException(username);
        final List<String> roles = new ArrayList<>();
        roles.add(user.getRole().toString());
        return new CustomUser(org.springframework.security.core.userdetails.User
                .withUsername(user.getLogin())
                .password(user.getPasswordHash())
                .roles(roles.toArray(new String[]{}))
                .build()).withUserId(user.getId());
    }

    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @PostConstruct
    private void init() {
        initUser("admin", "admin", Role.ADMIN);
        initUser("test", "test", Role.USUAL);
    }

    private void initUser(final String login, final String password, final Role role) {
        final User user = findByLogin(login);
        if (user != null) return;
        createUser(login, password, role);
    }

    @Transactional
    public void createUser(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        final String passwordHash = passwordEncoder.encode(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        userRepository.save(user);
    }

}
