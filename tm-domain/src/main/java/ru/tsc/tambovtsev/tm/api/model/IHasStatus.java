package ru.tsc.tambovtsev.tm.api.model;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.enumerated.Status;

public interface IHasStatus {

    @Nullable
    Status getStatus();

    void setStatus(@Nullable Status status);

}
